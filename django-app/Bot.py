import json
import jellyfish
import re, vk
from collections import defaultdict
from threading import Thread, Event
import os, datetime
from src.settings import MEDIA_ROOT
from abc import ABCMeta, abstractmethod, abstractproperty
from django.db import models
from django.forms import ModelForm


class Bot(object):
    __metaclass__=ABCMeta

    def __init__(self, token):
        self.AllowedForTasks = [55245649]
        self.RemindersS = {}
        self.REMINTERVALS = {}
        self.waked = False
        self.session = vk.Session()
        self.api = vk.API(self.session, v=5.85)
        self.token = token
        self.functions = {'%задача': self.addTask}

    @abstractmethod
    def getTasksForUser(self, user_id):
        pass

    @abstractmethod
    def getTask(self, time, user_id, task):
        pass

    @abstractmethod
    def getTasksForWaking(self, reminder):
        pass

    @abstractmethod
    def addTaskToBase(self, time, user_id, boss_id, task, comment, reminder):
        pass

    @abstractmethod
    def changeTaskStatus(self, time, user_id, task, status):
        pass

    @abstractmethod
    def addUser(self, user_id):
        pass

    def wakeUp(self):
      for rem in self.REMINTERVALS.keys():
          tasks = self.getTasksForWaking(rem)
          for task in tasks:
              self.RemindersS[task.TIME + task.TASK] = Event()
              name = self.api.users.get(access_token=self.token, user_ids= task.BOSS_ID)[0]
              name = name['first_name'] + ' ' +  name['last_name']
              r = Reminder( RemindersS[ task.TIME +  task.TASK], self.REMINTERVALS[rem],task.ID,
              name, task.TASK,task.COMMENT, self.token, self.api)
              r.start()
      self.waked = True

    def allowTasks(self, ids):
        for id in ids:
            self.AllowedForTasks.append(id)

    def process(self, from_id, peer_id, msg, payload, attach):
        if not msg.startswith('%'):
            if payload == None:
              return 'garbage'
            command = json.loads(payload)
            return self.button(from_id, command)

        selector = defaultdict(lambda: 'notfound',
                           self.functions)
    
        #parts = msg.split('\n')
        #print(parts)
        args = msg[0:msg.find('\n')+1:1]
        msg = msg.replace(args, '')
        args = args.replace('\n','')
        task = msg[0:msg.find('\n')+1:1]
        msg = msg.replace(task, '')
        task = task.replace('\n','')
        args = args.strip().split(' ')
        print(args, task, msg)
        return selector[args[0]](from_id, peer_id, args, task , msg, attach)
    
    def button(self, from_id, command):
        bmsg = ''
        if command['command'] == 'done':
           self.changeTaskStatus(command['time'], from_id, command['task'],'СДЕЛАНО')
           msg = 'Спасибо!'
           bmsg = 'выполнил задачу.'
        elif command['command'] == 'cancel':
           self.changeTaskStatus(command['time'], from_id, command['task'],'ОТМЕНЕНО')
           msg = 'Задача отклонена '
           bmsg = 'отклонил задачу '
        if command['time']+command['task'] in self.RemindersS.keys():
           self.RemindersS[command['time']+command['task']].set()   

        keyboard = {
           "one_time": False,
           "buttons": []
        }
        
        tasks = self.getTasksForUser(targetId)

        for task in tasks:
            t = task.TASK
            keyboard['buttons'].append([{
              "action": {
                "type": "text",
                "payload": "{\"command\": \"done\", \"task\": \"" + t + "\", \"time\": \"" + task.TIME + "\"}",
                "label": 'Сделано ' + t
              },
              "color": "positive"
            },
            {
              "action": {
                "type": "text",
                "payload": "{\"command\": \"cancel\", \"task\": \"" + t + "\", \"time\": \"" + task.TIME + "\"}",
                "label": 'Не могу сделать ' + t
              },
              "color": "negative"
            }
            ])

        name = self.api.users.get(access_token=self.token, user_ids=from_id)[0]
        name = name['first_name'] + ' ' +  name['last_name']
        curTask = self.getTask(command['time'], from_id, command['task'])

        self.api.messages.send(access_token=self.token, user_id=curTask.BOSS_ID, message=name + ' ' + bmsg +' "' + command['task'] + '".')
        self.api.messages.send(access_token=self.token, user_id=from_id, message=msg, keyboard=json.dumps(keyboard, ensure_ascii = False))
        return 'ok'


    def addTask(self, from_id, peer_id, args, task, comment, attach):
       task = task.replace('\n','')

       try:
          bossName = self.api.users.get(access_token=self.token, user_ids=from_id)[0]
          bossName = bossName['first_name'] + ' ' +  bossName['last_name']
          targetNick = str(args[1])[str(args[1]).find('id'):str(args[1]).find('|'):1].replace('id', '')
          print(targetNick)
          targetId = int(targetNick)
          if from_id not in self.AllowedForTasks and not (from_id == targetId):
            return 'not allowed'
          time = datetime.datetime.now().strftime("%Y-%m-%d-%H.%M.%S")
          userName = self.api.users.get(access_token=self.token, user_ids=targetId)[0]
          userName = userName['first_name'] + ' ' +  userName['last_name']
          self.addUser(from_id, userName)
          if len(args) > 2:
            if str(args[2]).upper().strip() in self.REMINTERVALS.keys():
              self.addTaskToBase(time, targetId, from_id, task, comment, 'ПОСТАВЛЕНА', str(args[2]).upper().strip())
              RemindersS[time + task] = Event()
              r = Reminder(RemindersS[time + task], self.REMINTERVALS[str(args[2]).upper().strip()],targetId, bossName, task, comment, self.token, self.api)
              r.start()
              self.api.messages.send(access_token=self.token, peer_id=peer_id, message='Важная задача "' + task + '" поставлена!')
            else:
              print('Не понял аргумента')
              self.addTaskToBase(time, targetId, from_id, task, comment, 'ПОСТАВЛЕНА', '')
          else:
              self.api.messages.send(access_token=self.token, peer_id=peer_id, message='Задача "' + task + '" поставлена!')
              self.addTaskToBase(time, targetId, from_id, task, comment, 'ПОСТАВЛЕНА', '')
          keyboard = {
            "one_time": False,
            "buttons": []
          }

          tasks = self.getTasksForUser(targetId)

          for task in tasks:
              t = task.TASK
              keyboard['buttons'].append([{
                "action": {
                  "type": "text",
                  "payload": "{\"command\": \"done\", \"task\": \"" + t + "\", \"time\": \"" + task.TIME + "\"}",
                  "label": 'Сделано ' + t
                },
                "color": "positive"
              },
              {
                "action": {
                  "type": "text",
                  "payload": "{\"command\": \"cancel\", \"task\": \"" + t + "\", \"time\": \"" + task.TIME + "\"}",
                  "label": 'Не могу сделать ' + t
                },
                "color": "negative"
              }
              ])

          kb = ''
          if attach == None or len(attach) == 0:
            self.api.messages.send(access_token=self.token, user_id=targetId, message=bossName + ' поставил вам задачу:' + '\n' + task + '\n' + comment, keyboard=json.dumps(keyboard, ensure_ascii = False))
          else:
            a = self.api.messages.send(access_token=self.token, user_id=targetId, message=bossName + ' поставил вам задачу:' + '\n' + task + '\n' + comment, keyboard=json.dumps(keyboard, ensure_ascii = False))
          print(args)
          return 'ok'
       except Exception as e:
          print('ОШИБКА ПРИ ПОСТАНОВКЕ ЗАДАЧИ: ' + str(e))
          self.api.messages.send(access_token=self.token, peer_id=peer_id, message='Не удалось поставить задачу!')

class Reminder(Thread):
      def __init__(self, event, interval, target_id, bossName, task, comment, token, api):
          Thread.__init__(self)
          self.stopped = event
          self.interval = interval
          self.target_id = target_id
          self.bossName = bossName
          self.task = task
          self.comment = comment
          self.token = token
          self.api = api

      def run(self):
          while not self.stopped.wait(self.interval):
              self.api.messages.send(access_token=self.token, user_id=self.target_id, message='Напоминаю про задачу!\n' + self.bossName + ' отметил ее как важную.\nЗадача: ' +self.task + '\nОписание:' + self.comment)
            # call a function

  
    
    

    
