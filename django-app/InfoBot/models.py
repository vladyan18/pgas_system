from django.db import models

class User(models.Model):
    ID = models.IntegerField(primary_key=True)
    NAME = models.CharField(max_length=100)

class Task(models.Model):
    TIME = models.CharField(max_length=30)
    USER_ID = models.OneToOneField(User,related_name="UID", on_delete='cascade')
    BOSS_ID = models.OneToOneField(User,related_name="BID",  on_delete='cascade')
    TASK = models.CharField(max_length=100)
    COMMENT = models.CharField(max_length=1000)
    REMINDER = models.CharField(max_length=30)


# Create your models here.
