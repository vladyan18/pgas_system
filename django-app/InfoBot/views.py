from django.shortcuts import render
from django.shortcuts import render
from uploader.models import UploadForm,Upload, UploadTable, UploadTableForm
from django.http import HttpResponseRedirect
from django.urls import reverse
import InfoBot
import token as infoToken
from src.settings import MEDIA_ROOT, BASE_DIR
from django.http import HttpResponse
from io import StringIO
import os, mimetypes, urllib
import datetime
from django.core.files.base import ContentFile
import vk, json
from django.views.decorators.csrf import csrf_exempt
# Create your views here.

print(infoToken.token)
infoBot = InfoBot.InfoBot(infoToken.token)
infoBot.allowTasks([137784015, 373044930])

@csrf_exempt
def infokomCallback(request):
    if request.method=="POST":
        body_unicode = request.body.decode('utf-8')
        print(body_unicode)
        data = json.loads(body_unicode)
        if data['type'] == 'confirmation':
            return HttpResponse('ca03177d')
        elif data['type'] == 'message_new':
            #try:
                user_id = data['object']['from_id']
                
                if not infoBot.waked: infoBot.wakeUp()
                payload = attach = None
                if 'attachments' in data['object'].keys() and len(data['object']['attachments'])>0:
                  attach = data['object']['attachments']
                if 'payload' in data['object'].keys():
                  payload = data['object']['payload']
                infoBot.process(user_id, data['object']['peer_id'], data['object']['text'], payload, attach) 
            #except Exception as e: print(e)
    return HttpResponse('ok')