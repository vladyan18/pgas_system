from django.apps import AppConfig


class InfobotConfig(AppConfig):
    name = 'InfoBot'
