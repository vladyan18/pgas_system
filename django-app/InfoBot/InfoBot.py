import Bot
from InfoBot import models

class InfoBot(Bot.Bot):
  def __init__(self, token):
    Bot.Bot.__init__(self, token)

  def getTasksForUser(self, user_id):
      tasks = models.Task.objects.filter(USER_ID=user_id)
      return tasks

  def getTask(self, time, user_id, task):
      task = models.Task.objects.get(TIME=time, USER_ID=user_id, task=task)
      return task

  def getTasksForWaking(self, reminder):
      tasks = models.Task.objects.filter(REMINDER=reminder).exclude(STATUS='СДЕЛАНО').exclude(STATUS='ОТМЕНЕНО')
      return tasks

  def addTaskToBase(self, time, user_id, boss_id, task, comment, reminder):
      task = models.Task.objects.create(TIME=time, USER_ID=user_id, BOSS_ID=boss_id, TASK=task, COMMENT=comment, REMINDER=reminder)

  def changeTaskStatus(self, time, user_id, task, status):
      task = models.Task.objects.get(TIME=time, USER_ID=user_id, task=task)
      task.STATUS = status
      task.save(update_fields=["STATUS"])

  def addUser(self, user_id, name):
      user = models.USER.objects.get(ID=user_id)
      if user == None:
          user = models.USER.objects.create(user_id, name)


