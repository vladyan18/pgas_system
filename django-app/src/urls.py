"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from uploader import views as uploader_views
from InfoBot import views as InfoBot_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', uploader_views.home, name='kritupload'),
    path('callback/', uploader_views.callback, name='cb'),
    path('secretarycallback/', uploader_views.secretaryCallback, name='scb'),
    path('finaltable/', uploader_views.finalTable, name='ft'),
    path('table/', uploader_views.table, name='tableupload')
    #path('infobot/', InfoBot_views.infokomCallback, name='infobot')
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


