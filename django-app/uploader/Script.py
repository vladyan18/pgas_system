import json
from openpyxl import load_workbook
from openpyxl.styles import PatternFill
from openpyxl.styles import colors
import jellyfish

def main(book, directory):
    systematic2 = ['9а','10в','11б']
    systematic3 = []
    wb = load_workbook(book)
    ws = wb['Лист1']

    charCol = 'NONE'
    ballCol = 'NONE'
    critCol = 'NONE'
    fioCol = 'NONE'
    cursCol = 'NONE'
    comCol = 'NONE'
    statCol = 'NONE'
    for row in ws.iter_rows(min_row=1, max_row=1):
        for cell in row:
            if cell.value == None: continue
            if cell.value.upper().strip() == 'КРИТЕРИЙ':
                critCol = cell.column
            elif cell.value.upper().strip() == 'ХАРАКТЕРИСТИКИ':
                charCol = cell.column
            elif cell.value.upper().strip() == 'БАЛЛ':
                ballCol = cell.column
            elif cell.value.upper().strip() == 'ФИО':
                fioCol = cell.column
            elif cell.value.upper().strip().find('КОММЕНТАРИЙ СИСТЕМЫ') != -1:
                comCol = cell.column
            elif cell.value.upper().strip().find('СТАТУС ДОСТИЖЕНИЯ') != -1:
                statCol = cell.column
            elif cell.value.upper().strip() == 'КУРС':
                cursCol = cell.column
    #исключение, если не найдены столбцы
            
    print(charCol, ballCol, critCol, fioCol, cursCol, comCol)

    with open(directory + 'Kriterii.json', encoding='utf-8') as outfile: #импорт критериев
        Table = json.load(outfile)

    krits = Table.keys()
    sKrits = []
    lKrits = []
    for key in krits:
        sKrits.append(str(key).split('(')[1].strip(')'))
        lKrits.append(str(key))

    class Ach(object):
        def __init__(self, points, pos):
            self.points = points
            self.pos = pos
            pass

        def writePoints(self, points, ws):
            ws[self.pos] = points
        

    class Student(object):
        """docstring"""
     
        def __init__(self):
            """Constructor"""
            self.achs = {}
            pass
        
        def addAch(self, krit, points, pos):
            if krit in self.achs.keys():
                self.achs[krit].append(Ach(points, pos))
            else:
                self.achs[krit] = [Ach(points, pos)]
                
        def countPoints(self, ws):
            for krit in self.achs.keys():
                achs = self.achs[krit]
                maximums = []
                i = 0
                if str(krit) in systematic2 and len(achs) < 2:
                    for a in achs: a.writePoints(0, ws)
                elif str(krit) in systematic3 and len(achs) < 3:
                    for a in achs: a.writePoints(0, ws)
                else:
                    while len(achs) > 0:
                        points = []
                        if i >= len(achs[0].points):
                          break
                        for j in range(len(achs)):
                          points.append(achs[j].points[i])
                        achs[points.index(max(points))].writePoints(max(points), ws)
                        achs.pop(points.index(max(points)))
                        if i < 5 or str(krit).upper().strip() == '7А':
                            i += 1        
    def normalise(string):
        l = string.upper().replace(',','').replace('(','').replace(')','').split(' ')
        
        return l

    def koeff(s1, s2):
        res = 0
        for s in s1:
            for t in s2:
                if jellyfish.jaro_winkler(s, t) > 0.8:
                    res += 1
                    break;
        return res

    students = {}
    for i in range(2, ws.max_row+1):
        if  ws[str(fioCol + str(i))].value == None: continue
        fio = ws[str(fioCol + str(i))].value.upper().strip()
        if fio not in students.keys():
            students[fio] = Student()
            
        curKrit = ws[str(critCol + str(i))].value.lower()
        data = Table[lKrits[sKrits.index(curKrit)]]
        cs = ws[str(charCol + str(i))].value
        if ws[str(statCol + str(i))].value == None: continue
        if cs != None and cs.upper() == 'НЕТ' or  ws[str(statCol + str(i))].value.upper().strip().find('НЕ ЗАЧТЕНО') != -1:
            ws[str(ballCol + str(i))] = '-'
            continue
        if cs != None :
            cs = cs.split(',')
            for c in range(len(cs)):
                if isinstance(data, list):
                    print('ИЗБЫТОЧНО' + cs[c])
                    ws[str(ballCol + str(i))] = 'УКАЗАНА ИЗБЫТОЧНАЯ ХАРАКТЕРИСТИКА ' + str(cs[c])
                    for rows in ws.iter_rows(min_row=i, max_row=i):
                      for cell in rows:
                        cell.fill = PatternFill(fill_type='solid', start_color="008888", end_color="008888")
                    ws[str(comCol + str(i))] = 'избыточная характеристика: ' + str(cs[c])
                    continue

                cs[c] = cs[c].strip()
                curArg = 0
                args = list(filter(lambda x: str(x).upper().strip()==cs[c].upper().strip(), list(data.keys())) )
                if len(args) == 0:
                  args = list(filter(lambda x: str(x).upper().find(cs[c].upper()) != -1, list(data.keys())) )
                if len(args) > 1: 
                    print('НЕОДНОЗНАЧНАЯ ХАРАКТЕРИСТИКА')
                    for j in range(len(args)):
                       if len(args[j]) < len(args[curArg]): curArg = j
                    for rows in ws.iter_rows(min_row=i, max_row=i, min_col=1):
                      for cell in rows:
                        cell.fill = PatternFill(start_color="FFFF00", end_color="FFFF00", fill_type = "solid")
                    ws[str(comCol + str(i))] = 'НЕОДНОЗНАЧНАЯ ХАРАКТЕРИСТИКА: ' + str(cs[c]) + 'Выбрано: ' + args[curArg]
                print(args)
                if len(args) == 0:
                    print('ТОЧНАЯ ХАРАКТЕРИСТИКА ' + cs[c] + ' НЕ НАЙДЕНА')

                    args = list(filter(lambda x: koeff(normalise(str(x)), normalise(cs[c])) != 0,list(data.keys())))
                    args = sorted(args, key=lambda x:koeff(normalise(str(x)), normalise(cs[c])), reverse=True)
                    if len(args) > 1:
                        print('НЕОДНОЗНАЧНАЯ ХАРАКТЕРИСТИКА')
                        for j in range(len(args)):
                          if len(args[j]) < len(args[curArg]) and koeff(normalise(args[j]), normalise(cs[c])) > koeff(normalise(args[curArg]), normalise(cs[c])) : curArg = i
                        for rows in ws.iter_rows(min_row=i, max_row=i, min_col=1):
                          for cell in rows:
                            cell.fill = PatternFill(start_color="FFFF00", end_color="FFFF00", fill_type = "solid")
                        ws[str(comCol + str(i))] = 'НЕОДНОЗНАЧНАЯ ХАРАКТЕРИСТИКА: ' + str(cs[c]) + 'Выбрано: ' + args[curArg]
                    if len(args) == 0:
                        print('ХАРАКТЕРИСТИКА ' + cs[c] + ' НЕ НАЙДЕНА')
                        for rows in ws.iter_rows(min_row=i, max_row=i, min_col=1):
                          for cell in rows:
                            cell.fill = PatternFill(start_color="FF0000", end_color="FF0000", fill_type = "solid")

                        ws[str(ballCol + str(i))] = 'УКАЗАНА НЕПРАВИЛЬНАЯ ХАРАКТЕРИСТИКА ' + str(cs[c])
                        ws[str(comCol + str(i))] = 'УКАЗАНА НЕПРАВИЛЬНАЯ ХАРАКТЕРИСТИКА ' + str(cs[c]) + 'Возможные варианты: ' + str(data.keys())
                        data = None
                        break;
                    else:
                        print('ЗА ХАРАКТЕРИСТИКУ ПРИНЯТО: ' + args[curArg])
                        data = data[args[curArg]]
                else:
                    data = data[args[curArg]]
        #print(data)
        if data != None:
            if isinstance(data, list):
                if curKrit == '7а' and str(ws[str(cursCol + str(i))].value) == '1':
                    students[fio].addAch(curKrit, [0], str(ballCol + str(i)))
                else:
                    students[fio].addAch(curKrit, data, str(ballCol + str(i)))
            else:
                ws[str(ballCol + str(i))] = 'ХАРАКТЕРИСТИКА НЕДОСТАТОЧНА'
                for rows in ws.iter_rows(min_row=i, max_row=i, min_col=1):
                  for cell in rows:
                    cell.fill = PatternFill(start_color="FF0000", end_color="FF0000", fill_type = "solid")
        
    for st in students.keys():
        students[st].countPoints(ws)

    wb.save(directory + 'Result_scores.xlsx')
