from django.shortcuts import render
from uploader.models import UploadForm,Upload, UploadTable, UploadTableForm
from django.http import HttpResponseRedirect
from django.urls import reverse
from uploader import kappa123
from uploader import Script
from uploader import makeTable
from uploader import makeFinalTable
from uploader import Sender
from uploader import Secretary
from src.settings import MEDIA_ROOT, BASE_DIR
from django.http import HttpResponse
from io import StringIO
import os, mimetypes, urllib
import datetime
from django.core.files.base import ContentFile
import vk, json

from django.views.decorators.csrf import csrf_exempt

# Create your views here.

def home(request):
    if request.method=="POST":
        kritF = UploadForm(request.POST, request.FILES)       
        if kritF.is_valid():
            
            folder = datetime.datetime.now().strftime("%Y-%m-%d-%H.%M.%S")
            filename = request.FILES.getlist('Критерий')[0].name
            try:
                os.mkdir(os.path.join(MEDIA_ROOT, 'media/' + folder))
            except:
                pass
            full_filename = os.path.join(MEDIA_ROOT, 'media/' + folder + '/', filename)
            
 
            #kritF.save()
            file_content = ContentFile( request.FILES.getlist('Критерий')[0].read())
            fout = open(full_filename, 'wb+')
            # Iterate through the chunks.
            for chunk in file_content.chunks():
                fout.write(chunk)
            fout.close()
            kappa123.main(os.path.join(MEDIA_ROOT,'media/' + folder + '/'+ filename), os.path.join(MEDIA_ROOT, 'media/' + folder + '/'))

            filename1 = request.FILES.getlist('Достижения')[0].name
            file_content1 = ContentFile( request.FILES.getlist('Достижения')[0].read())
            fout = open(os.path.join(MEDIA_ROOT, 'media/' + folder + '/', filename1), 'wb+')
            # Iterate through the chunks.
            for chunk in file_content1.chunks():
                fout.write(chunk)
            fout.close()
            Script.main(os.path.join(MEDIA_ROOT,'media/' + folder + '/'+ filename1), os.path.join(MEDIA_ROOT, 'media/' + folder + '/'))

            with open(os.path.join(MEDIA_ROOT,'media/' + folder + '/Result_scores.xlsx'), 'rb') as fh:
                response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=Result.xlsx'
                return response
        
    else:
        kritF=UploadForm()
    krits=Upload.objects.all().order_by('-upload_date')
    return render(request,'home.html',{'form':kritF,'krits':krits})


def table(request):
    if request.method=="POST":
        kritF = UploadTableForm(request.POST, request.FILES)       
        if kritF.is_valid():
            folder = datetime.datetime.now().strftime("%Y-%m-%d-%H.%M.%S")
            filename = request.FILES.getlist('Таблица')[0].name
            try:
                os.mkdir(os.path.join(MEDIA_ROOT, 'media/' + folder))
            except:
                pass
            full_filename = os.path.join(MEDIA_ROOT, 'media/' + folder + '/', filename)
            
 
            #kritF.save()
            file_content = ContentFile( request.FILES.getlist('Таблица')[0].read())
            fout = open(full_filename, 'wb+')
            # Iterate through the chunks.
            for chunk in file_content.chunks():
                fout.write(chunk)
            fout.close()
            makeFinalTable.main( os.path.join(MEDIA_ROOT,'media/' + folder + '/'+ filename), os.path.join(MEDIA_ROOT, 'media/' + folder + '/'))
            with open(os.path.join(MEDIA_ROOT,'media/' + folder + '/FinalResult.xlsx'), 'rb') as fh:
                response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=FinalResult.xlsx'
                return response

    else:
        kritF=UploadTableForm()
    krits=UploadTable.objects.all().order_by('-upload_date')
    return render(request,'finalTable.html',{'form':kritF,'krits':krits})

def finalTable(request):
    if request.method=="POST":
        kritF = UploadTableForm(request.POST, request.FILES)       
        if kritF.is_valid():
            folder = datetime.datetime.now().strftime("%Y-%m-%d-%H.%M.%S") + 'FINAL'
            filename = request.FILES.getlist('Таблица')[0].name
            try:
                os.mkdir(os.path.join(MEDIA_ROOT, 'media/' + folder))
            except:
                pass
            full_filename = os.path.join(MEDIA_ROOT, 'media/' + folder + '/', filename)
            
 
            #kritF.save()
            file_content = ContentFile( request.FILES.getlist('Таблица')[0].read())
            fout = open(full_filename, 'wb+')
            # Iterate through the chunks.
            for chunk in file_content.chunks():
                fout.write(chunk)
            fout.close()
            makeFinalTable.main(os.path.join(MEDIA_ROOT,'media/PMtemplate.xlsx') ,os.path.join(MEDIA_ROOT,'media/' + folder + '/'+ filename), os.path.join(MEDIA_ROOT, 'media/' + folder + '/'))
            with open(os.path.join(MEDIA_ROOT,'media/' + folder + '/FinalResult.xlsx'), 'rb') as fh:
                response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=FinalResult.xlsx'
                return response

    else:
        kritF=UploadTableForm()
    krits=UploadTable.objects.all().order_by('-upload_date')
    return render(request,'table.html',{'form':kritF,'krits':krits})

@csrf_exempt
def callback(request):
    if request.method=="POST":
        body_unicode = request.body.decode('utf-8')
        print(body_unicode)
        data = json.loads(body_unicode)
        if data['type'] == 'confirmation':
            return HttpResponse('c77efb32')
        elif data['type'] == 'message_new':
            session = vk.Session()
            api = vk.API(session, v=5.85)
            user_id = data['object']['user_id']
            token = '1cc69fe8ebc40df2e62c1cbf1cf563e1dd55d8911708aeec7ded02b6c6387703ad2be5d17c851acd2a041'
            if user_id == 55245649:
                txt = data['object']['body']
                if txt == 'ОТПРАВИТЬ':
                    Sender.main(os.path.join(MEDIA_ROOT,'media/Result.xlsx'), os.path.join(MEDIA_ROOT,'media/Table.xlsx'), api) 
    return HttpResponse('ok')

@csrf_exempt
def secretaryCallback(request):
    if request.method=="POST":
        body_unicode = request.body.decode('utf-8')
        print(body_unicode)
        data = json.loads(body_unicode)
        if data['type'] == 'confirmation':
            return HttpResponse('418e6fc5')
        elif data['type'] == 'message_new':
            session = vk.Session()
            api = vk.API(session, v=5.85)
            user_id = data['object']['from_id']
            token = 'e99ac22f812f869efdd4d51dd824710e3d527f9a78cc3468bb55afed07d2a281e198e67ef10738dad9af6'
            if not Secretary.waked: Secretary.wakeUp(token, api)
            attach = None
            if 'attachments' in data['object'].keys() and len(data['object']['attachments'])>0:
              attach = data['object']['attachments']
              print('Есть приложение!')
            if 'payload' in data['object'].keys():
              answ = Secretary.process(user_id, data['object']['peer_id'], data['object']['text'], api, token, data['object']['payload'], attach)
            else:
              answ = Secretary.process(user_id, data['object']['peer_id'], data['object']['text'], api, token, None, attach)
            print(answ) 
    return HttpResponse('ok')


