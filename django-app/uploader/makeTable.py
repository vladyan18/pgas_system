import json
from openpyxl import load_workbook, Workbook
import jellyfish
import re

def main(book, directory):
    wb = load_workbook(book)
    ws = wb['Лист1']

    typeCol = ''
    fioCol = ''
    cursCol = ''
    critCols = {}

    for row in ws.iter_rows(min_row=1, max_row=1):
        for cell in row:
            if cell.value == None: continue
            if cell.value.upper().strip().find('ФИО') != -1:
                fioCol = cell.column
            elif cell.value.upper().strip().find('ТИП') != -1:
                typeCol = cell.column
            elif cell.value.upper().strip().find('КУРС') != -1:
                cursCol = cell.column
                
            elif cell.value.upper().strip().find('7А') != -1:
                critCols['7А'] = cell.column
            elif cell.value.upper().strip().find('7Б') != -1:
                critCols['7Б'] = cell.column
            elif cell.value.upper().strip().find('7В') != -1:
                critCols['7В'] = cell.column
            elif cell.value.upper().strip().find('8А') != -1:
                critCols['8А'] = cell.column
            elif cell.value.upper().strip().find('8Б') != -1:
                critCols['8Б'] = cell.column
            elif cell.value.upper().strip().find('9А') != -1:
                critCols['9А'] = cell.column
            elif cell.value.upper().strip().find('9Б') != -1:
                critCols['9Б'] = cell.column
            elif cell.value.upper().strip().find('10А') != -1:
                critCols['10А'] = cell.column
            elif cell.value.upper().strip().find('10Б') != -1:
                critCols['10Б'] = cell.column
            elif cell.value.upper().strip().find('10В') != -1:
                critCols['10В'] = cell.column
            elif cell.value.upper().strip().find('11А') != -1:
                critCols['11А'] = cell.column
            elif cell.value.upper().strip().find('11Б') != -1:
                critCols['11Б'] = cell.column
            elif cell.value.upper().strip().find('11В') != -1:
                critCols['11В'] = cell.column    
    #исключение, если не найдены столбцы
            
    print(fioCol, typeCol, cursCol, critCols)

    outbook = Workbook()
    outbookSheet = outbook.active
    outbookSheet.title = 'Лист1'
    #for row in ws.iter_rows(min_row=1, max_row=1):
    #    outbookSheet.append(row)
    outbookSheet[str('A' + str(1))] = 'ФИО'
    outbookSheet[str('B' + str(1))] = 'Курс'
    outbookSheet[str('C' + str(1))] = 'Тип обучения'
    outbookSheet[str('D' + str(1))] = 'Критерий'
    outbookSheet[str('E' + str(1))] = 'Достижение'
    outbookSheet[str('F' + str(1))] = 'Комментарий'
    outbookSheet[str('G' + str(1))] = 'Характеристики'
    outbookSheet[str('H' + str(1))] = 'Балл'

    outRow = 2
    for i in range(2, ws.max_row+1):
        if ws[str(fioCol + str(i))].value == None: continue
        fio = ws[str(fioCol + str(i))].value.strip()
        curs = ws[str(cursCol + str(i))].value.strip()
        typeE = ws[str(typeCol + str(i))].value.strip()
        for krit in critCols.keys():
            if ws[str(critCols[krit] + str(i))].value == None: continue
            if str(ws[str(critCols[krit] + str(i))].value).upper().strip() not in ['','НЕТ','-']:
                achs = str(ws[str(critCols[krit] + str(i))].value).strip().split('\n#')
                if len(achs) == 1:
                    achs = str(ws[str(critCols[krit] + str(i))].value).strip().split('#')
                if len(achs) == 1:
                    achs = re.split(r'\n\d{1,2}\.', str(ws[str(critCols[krit] + str(i))].value).strip())
                for ach in achs:
                    if (ach.upper().strip() not in ['ДА', 'ДА.','ДА\n','ДА.\n','ДА,','ДА,\n']) or krit == '7А':
                        outbookSheet[str('A' + str(outRow))] = fio
                        outbookSheet[str('B' + str(outRow))] = curs
                        outbookSheet[str('C' + str(outRow))] = typeE
                        outbookSheet[str('D' + str(outRow))] = krit
                        outbookSheet[str('E' + str(outRow))] = ach
                        outRow += 1
    outbook.save(directory + '/NiceTable.xlsx')
            

