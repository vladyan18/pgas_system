import json
from openpyxl import load_workbook, Workbook
import jellyfish
import re, vk
import time

def main(book, eBook, api):
    wb = load_workbook(book)
    ws = wb['Лист1']
    token = '1cc69fe8ebc40df2e62c1cbf1cf563e1dd55d8911708aeec7ded02b6c6387703ad2be5d17c851acd2a041'
    typeCol = 'NONE'
    fioCol = 'NONE'
    cursCol = 'NONE'
    achCol = 'NONE'
    comCol = 'NONE'
    sendedCol = 'NONE'
    statCol = 'NONE'
    charCol = 'NONE'
    pointsCol = 'NONE'
    kritCol = 'NONE'

    for row in ws.iter_rows(min_row=1, max_row=1):
        for cell in row:
            if cell.value == None: continue
            if cell.value.upper().strip().find('ФИО') != -1:
                fioCol = cell.column
            elif cell.value.upper().strip().find('ТИП') != -1:
                typeCol = cell.column
            elif cell.value.upper().strip().find('КУРС') != -1:
                cursCol = cell.column
            elif cell.value.upper().strip().find('ДОСТИЖЕНИЕ') != -1:
                achCol = cell.column
            elif cell.value.upper().strip() == 'КОММЕНТАРИЙ':
                comCol = cell.column
            elif cell.value.upper().strip().find('СТАТУС ДОСТИЖЕНИЯ') != -1:
                statCol = cell.column
            elif cell.value.upper().strip().find('ХАРАКТЕРИСТИКИ') != -1:
                charCol = cell.column
            elif cell.value.upper().strip().find('БАЛЛ') != -1:
                pointsCol = cell.column
            elif cell.value.upper().strip().find('КРИТЕРИЙ') != -1:
                kritCol = cell.column


    #исключение, если не найдены столбцы
            
    print(fioCol, typeCol, cursCol, achCol, comCol)

    ew = load_workbook(eBook)
    es = ew['Лист1']
    vkCol = ''
    efioCol = ''
    for row in es.iter_rows(min_row=1, max_row=1):
        for cell in row:
            if cell.value == None: continue
            if cell.value.upper().strip().find('ФИО') != -1:
                efioCol = cell.column
            elif cell.value.upper().strip().find('ССЫЛКА') != -1:
                vkCol = cell.column
            elif cell.value.upper().strip().find('ОТОСЛАНО') != -1:
                sendedCol = cell.column
    links = {}
    for i in range(2, es.max_row+1):
        fio = es[str(efioCol + str(i))].value
        if fio == None: continue
        fio = fio.strip()
        vkL = es[str(vkCol + str(i))].value.strip()
        vkL = vkL[vkL.rfind('/')+1: len(vkL):1]
        if re.match(r'id\d{1,15}', vkL):
            vkL = int(vkL.replace('id', ''))
        links[fio] = vkL

    messages = {}
    outRow = 2
    fio = ''
    pfio = ''
    for i in range(2, ws.max_row+1):
        if  pfio != fio:
            if pfio != '' and pfio != fio and es[str(sendedCol + str(outRow))].value != 'ДА':
                try:
                  if pfio in links.keys():
                    api.messages.send(access_token=token, user_id=api.users.get(access_token=token, user_ids=str(links[pfio]))[0]['id'], message=messages[pfio])
                    es[str(sendedCol + str(outRow))] = 'ДА'
                    time.sleep(1)
                  else:
                    print('НЕТ ССЫЛКИ НА ЧЕЛОВЕКА: ' + pfio)
                except:
                  print('НЕ УДАЛОСЬ ОТПРАВИТЬ СООБЩЕНИЕ ЧЕЛОВЕКУ: ' + pfio)
                  es[str(sendedCol + str(outRow))] = 'ОШИБКА'
            pfio = fio
            outRow += 1
        fio = ws[str(fioCol + str(i))].value
        if fio not in links.keys(): 
          continue
        if fio not in messages.keys(): 
           messages[fio] = 'Привет! Я бот Стипендиальной комиссии и мне велено рассказать тебе про результат проверки твоих достижений!\n ПОЖАЛУЙСТА, ПИШИ СЮДА, ТОЛЬКО ЕСЛИ СОВЕРШЕННО УВЕРЕН, ЧТО СИСТЕМА ДОПУСТИЛА ОШИБКУ ПРИ ПОДСЧЕТЕ!'
        ach = ws[str(achCol + str(i))].value
        com = ws[str(comCol + str(i))].value
        krit = ws[str(kritCol + str(i))].value
        status = ws[str(statCol + str(i))].value
        chars = ws[str(charCol + str(i))].value
        pointsT = ws[str(pointsCol + str(i))].value
        try:
          messages[fio] = messages[fio] + '\n\nКРИТЕРИЙ: ' + krit
          messages[fio] =  messages[fio] + '\nДОСТИЖЕНИЕ: "' + ach + '"\nCТАТУС: ' +  status
          if chars != None: messages[fio] = messages[fio] + '\nХАРАКТЕРИСТИКИ: ' + chars
          if pointsT != None: messages[fio] = messages[fio]  + '\nБАЛЛ: ' + str(pointsT)
          if com != None: messages[fio] = messages[fio] + '\nКОММЕНТАРИЙ: ' + com
        except:
          messages[fio] = messages[fio] + '\n\nУПС! ЧТО-ТО ПОШЛО НЕ ТАК! СООБЩИ КОМИССИИ!'
    if pfio != '' and es[str(sendedCol + str(outRow))].value != 'ДА':
      try:
         if pfio in links.keys():
           print('отсылаем сообщение человеку: ' + pfio + ', текст: ' + messages[pfio])
           api.messages.send(access_token=token, user_id=api.users.get(access_token=token, user_ids=str(links[pfio]))[0]['id'], message=messages[pfio])
           es[str(sendedCol + str(outRow))] = 'ДА'
           time.sleep(1)
           outRow += 1
         else: 
           print('НЕТ ССЫЛКИ НА ЧЕЛОВЕКА: ' + pfio)
           outRow += 1
      except:
         print('НЕ УДАЛОСЬ ОТПРАВИТЬ СООБЩЕНИЕ ЧЕЛОВЕКУ: ' + pfio)
         es[str(sendedCol + str(outRow))] = 'ОШИБКА'
         outRow += 1

        
    ew.save(eBook)
            

