import json
from openpyxl import load_workbook, Workbook
import jellyfish
import re

def main(template, table, directory):
    wb = load_workbook(template)
    ws = wb['Лист1']

    typeCol = ''
    fioCol = ''
    cursCol = ''
    critCols = {}

    for row in ws.iter_rows(min_row=1, max_row=3):
        for cell in row:
            if cell.value == None: continue
            if cell.value.upper().strip().find('Ф.И.О') != -1:
                fioCol = cell.column
            elif cell.value.upper().strip().find('УРОВЕНЬ') != -1:
                typeCol = cell.column
            elif cell.value.upper().strip().find('КУРС') != -1:
                cursCol = cell.column
                
            elif cell.value.upper().strip().find('7А') != -1:
                critCols['7А'] = cell.column
            elif cell.value.upper().strip().find('7Б') != -1:
                critCols['7Б'] = cell.column
            elif cell.value.upper().strip().find('7В') != -1:
                critCols['7В'] = cell.column
            elif cell.value.upper().strip().find('8А') != -1:
                critCols['8А'] = cell.column
            elif cell.value.upper().strip().find('8Б') != -1:
                critCols['8Б'] = cell.column
            elif cell.value.upper().strip().find('9А') != -1:
                critCols['9А'] = cell.column
            elif cell.value.upper().strip().find('9Б') != -1:
                critCols['9Б'] = cell.column
            elif cell.value.upper().strip().find('10А') != -1:
                critCols['10А'] = cell.column
            elif cell.value.upper().strip().find('10Б') != -1:
                critCols['10Б'] = cell.column
            elif cell.value.upper().strip().find('10В') != -1:
                critCols['10В'] = cell.column
            elif cell.value.upper().strip().find('11А') != -1:
                critCols['11А'] = cell.column
            elif cell.value.upper().strip().find('11Б') != -1:
                critCols['11Б'] = cell.column
            elif cell.value.upper().strip().find('11В') != -1:
                critCols['11В'] = cell.column    
    #исключение, если не найдены столбцы
            
    print(fioCol, typeCol, cursCol, critCols)

    tb = load_workbook(table)
    ts = tb['Лист1']
    tbCols = {}
    #for row in ws.iter_rows(min_row=1, max_row=1):
    #    ts.append(row)
    for row in ts.iter_rows(min_row=1, max_row=1):
        for cell in row:
            if cell.value == None: continue
            if cell.value.upper().strip() == 'КРИТЕРИЙ':
                tbCols['krit'] = cell.column
            elif cell.value.upper().strip() == 'ХАРАКТЕРИСТИКИ':
                tbCols['chars'] = cell.column
            elif cell.value.upper().strip() == 'БАЛЛ':
                tbCols['point'] = cell.column
            elif cell.value.upper().strip().find('ФИО') != -1:
                tbCols['fio'] = cell.column
            elif cell.value.upper().strip().find('ТИП') != -1:
                tbCols['type'] = cell.column
            elif cell.value.upper().strip().find('КУРС') != -1:
                tbCols['curs'] = cell.column
    print(tbCols)
    outRow = 5
    students = {}
    for i in range(2, ts.max_row+1):
        if ts[str(tbCols['fio'] + str(i))].value == None or  ts[str(tbCols['type'] + str(i))].value == None or ts[str(tbCols['krit'] + str(i))].value == None: continue
        if ts[str(tbCols['point'] + str(i))].value == None or  ts[str(tbCols['curs'] + str(i))].value == None: continue

        fio = ts[str(tbCols['fio']+ str(i))].value.strip()
        curs = ts[str(tbCols['curs'] + str(i))].value
        typeE = ts[str(tbCols['type'] + str(i))].value.strip()
        krit = ts[str(tbCols['krit'] + str(i))].value.strip().upper()
        if ts[str(tbCols['point'] + str(i))].value == '-':
          point = 0
        else:
          point = float(ts[str(tbCols['point'] + str(i))].value)
        if fio not in students.keys():
            students[fio] = Student(fio, curs, typeE)
        students[fio].addAch(krit, point)

    for krit in critCols.keys():
        for st in students.keys():
            print('считаем ' + krit)
            students[st].countPoints(str(krit))
    rating = []
    for st in students.keys():
            students[st].countSum()
            rating.append(students[st])
    rating = sorted(rating, key=lambda x: (x.summa, x.points['7А'], x.points['7Б'], x.points['7В'], x.points['8А'], x.points['8Б'], x.points['9А'], x.points['9Б'],x.points['10А'], x.points['10Б'], x.points['10В'], x.points['11А'], x.points['11Б'], x.points['11В']), reverse=True)
    j = 1
    for st in rating:
      ws.append([j, st.fio, st.typeE, st.curs, st.points['7А'], st.points['7Б'] , st.points['7В'] , st.points['8А'] , st.points['8Б'] , st.points['9А'] , st.points['9Б'] , st.points['10А'] , st.points['10Б'] , st.points['10В'] , st.points['11А'] , st.points['11Б'] , st.points['11В'], st.summa]) 
      j += 1
    wb.save(directory + '/FinalResult.xlsx')

class Ach(object):
    def __init__(self, krit, point):
        self.krit = krit
        self.point = point

class Student(object):
    def __init__(self, fio, curs, typeE):
        self.fio = fio
        self.achs = []
        self.curs = curs
        self.typeE = typeE
        self.points = {}
        self.summa = 0
    def addAch(self, krit, point):
        self.achs.append(Ach(krit, point))

    def countSum(self):
        self.summa = 0
        for krit in self.points.keys():
            self.summa += self.points[krit]
            
    def countPoints(self, krit):
        point = 0
        for ach in self.achs:
            if ach.krit == krit:
                point += ach.point
        self.points[krit] = point
        return point
    

