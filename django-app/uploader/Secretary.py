import json
from openpyxl import load_workbook, Workbook
import jellyfish
import re, vk
from collections import defaultdict
from threading import Thread, Event
import os, datetime
from src.settings import MEDIA_ROOT

AllowedForTasks = [55245649,5394269,64004147]
RemindersS = {}
REMINTERVAL = 60*60
waked = False

def wakeUp(token, api):
  wb = load_workbook(os.path.join(MEDIA_ROOT, 'TaskBook.xlsx'))
  ws = wb['Лист1']
  ccols = {}
  for row in ws.iter_rows(min_row=1, max_row=1):
    for cell in row:
        if cell.value == None: continue
        if cell.value.upper().strip()== 'ID':
            ccols['id']  = cell.column
        elif cell.value.upper().strip().find('BOSS_ID') != -1:
            ccols['boss_id']  = cell.column
        elif cell.value.upper().strip().find('TASK') != -1:
            ccols['task']  = cell.column
        elif cell.value.upper().strip().find('STATUS') != -1:
            ccols['status'] = cell.column
        elif cell.value.upper().strip().find('TIME') != -1:
            ccols['time'] = cell.column
        elif cell.value.upper().strip().find('REMINDER') != -1:
            ccols['reminder'] = cell.column
        elif cell.value.upper().strip().find('COMMENT') != -1:
            ccols['comment'] = cell.column

  for i in range(2, ws.max_row+1):
    if ws[str(ccols['reminder'] + str(i))].value == 'ВАЖНО' and ws[str(ccols['status'] + str(i))].value not in ['СДЕЛАНО','ОТМЕНЕНО']:     
      RemindersS[ ws[str(ccols['time'] + str(i))].value +  ws[str(ccols['task'] + str(i))].value ] = Event()
      name = api.users.get(access_token=token, user_ids= ws[str(ccols['boss_id'] + str(i))].value)[0]
      name = name['first_name'] + ' ' +  name['last_name']

      r = Reminder( RemindersS[ ws[str(ccols['time'] + str(i))].value +  ws[str(ccols['task'] + str(i))].value ], REMINTERVAL,ws[str(ccols['id'] + str(i))].value,
      name, ws[str(ccols['task'] + str(i))].value,ws[str(ccols['comment'] + str(i))].value, token, api)
      r.start()
  waked = True


def process(from_id, peer_id, msg, api, token, payload, attach):
    if not msg.startswith('%'):
        if payload == None:
          return 'garbage'
        command = json.loads(payload)
        return button(from_id, command, token, api)

    selector = defaultdict(lambda: 'notfound',
                       {'%задача': addTask})
    
    #parts = msg.split('\n')
    #print(parts)
    args = msg[0:msg.find('\n')+1:1]
    msg = msg.replace(args, '')
    args = args.replace('\n','')
    task = msg[0:msg.find('\n')+1:1]
    msg = msg.replace(task, '')
    task = task.replace('\n','')
    args = args.strip().split(' ')
    print(args, task, msg)
    return selector[args[0]](from_id, peer_id, args, task , msg, api, token, attach)
    
def button(from_id, command, token, api):
   wb = load_workbook(os.path.join(MEDIA_ROOT, 'TaskBook.xlsx'))
   ws = wb['Лист1']
   columns = {}
   for row in ws.iter_rows(min_row=1, max_row=1):
      for cell in row:
          if cell.value == None: continue
          if cell.value.upper().strip()== 'ID':
              columns['id']  = cell.column
          elif cell.value.upper().strip().find('BOSS_ID') != -1:
              columns['boss_id']  = cell.column
          elif cell.value.upper().strip().find('TASK') != -1:
              columns['task']  = cell.column
          elif cell.value.upper().strip().find('STATUS') != -1:
              columns['status'] = cell.column
          elif cell.value.upper().strip().find('TIME') != -1:
              columns['time'] = cell.column
          elif cell.value.upper().strip().find('REMINDER') != -1:
              columns['reminder'] = cell.column


   for i in range(2, ws.max_row+1):
        if ws[str(columns['id'] + str(i))].value == from_id and ws[str(columns['task'] + str(i))].value == command['task'] and ws[str(columns['time'] + str(i))].value == command['time']:
          bmsg = ''
          if command['command'] == 'done':
            ws[str(columns['status'] + str(i))] = 'СДЕЛАНО'
            msg = 'Спасибо!'
            bmsg = 'выполнил задачу.'
          elif command['command'] == 'cancel':
            ws[str(columns['status'] + str(i))] = 'ОТМЕНЕНО'
            msg = 'Задача отклонена '
            bmsg = 'отклонил задачу '
          if command['time']+command['task'] in RemindersS.keys():
            RemindersS[command['time']+command['task']].set()

          keyboard = {
            "one_time": False,
            "buttons": []
          }

          tasks = []
          for j in range(2, ws.max_row+1):
             if ws[str(columns['id'] + str(j))].value == from_id and ws[str(columns['status'] + str(j))].value not in ['СДЕЛАНО','ОТМЕНЕНО']:
               t = ws[str(columns['task'] + str(j))].value
               keyboard['buttons'].append([{
                 "action": {
                   "type": "text",
                   "payload": "{\"command\": \"done\", \"task\": \"" + t + "\", \"time\": \"" + ws[str(columns['time'] + str(j))].value + "\"}",
                   "label": 'Сделано ' + t
                 },
                 "color": "positive"
               },
               {
                 "action": {
                   "type": "text",
                   "payload": "{\"command\": \"cancel\", \"task\": \"" + t + "\", \"time\": \"" + ws[str(columns['time'] + str(j))].value + "\"}",
                   "label": 'Не могу сделать ' + t
                 },
                 "color": "negative"
               }
               ])

          name = api.users.get(access_token=token, user_ids=from_id)[0]
          name = name['first_name'] + ' ' +  name['last_name']
          print('Boss id:' + str(ws[str(columns['boss_id'] + str(i))].value))
          api.messages.send(access_token=token, user_id=int(ws[str(columns['boss_id'] + str(i))].value), message=name + ' ' + bmsg +' "' + command['task'] + '".')
          api.messages.send(access_token=token, user_id=from_id, message=msg, keyboard=json.dumps(keyboard, ensure_ascii = False))
          break

   wb.save(os.path.join(MEDIA_ROOT, 'TaskBook.xlsx'))
   return 'ok'


def addTask(from_id, peer_id, args, task, comment, api, token, attach):
   task = task.replace('\n','')
   wb = load_workbook(os.path.join(MEDIA_ROOT, 'TaskBook.xlsx'))
   ws = wb['Лист1'] 
   columns = {}
   for row in ws.iter_rows(min_row=1, max_row=1):
      for cell in row:
          if cell.value == None: continue
          if cell.value.upper().strip() == 'ID':
              columns['id']  = cell.column
          elif cell.value.upper().strip().find('BOSS_ID') != -1:
              columns['boss_id']  = cell.column
          elif cell.value.upper().strip().find('TASK') != -1:
              columns['task']  = cell.column
          elif cell.value.upper().strip().find('STATUS') != -1:
              columns['status'] = cell.column
          elif cell.value.upper().strip().find('COMMENT') != -1:
              columns['comment'] = cell.column
          elif cell.value.upper().strip().find('TIME') != -1:
              columns['time'] = cell.column



   try:
      bossName = api.users.get(access_token=token, user_ids=from_id)[0]
      bossName = bossName['first_name'] + ' ' +  bossName['last_name']
      targetNick = str(args[1])[str(args[1]).find('id'):str(args[1]).find('|'):1].replace('id', '')
      print(targetNick)
      targetId = int(targetNick)
      if from_id not in AllowedForTasks and not (from_id == targetId):
        return 'not allowed'
      time = datetime.datetime.now().strftime("%Y-%m-%d-%H.%M.%S")

      if len(args) > 2:
        if str(args[2]).upper().strip() == 'ВАЖНО':
          ws.append([time, targetId, from_id, task, comment, 'ПОСТАВЛЕНА', 'ВАЖНО'])
          RemindersS[time + task] = Event()
          r = Reminder(RemindersS[time + task], REMINTERVAL,targetId, bossName, task, comment, token, api)
          r.start()
          api.messages.send(access_token=token, peer_id=peer_id, message='Важная задача "' + task + '" поставлена!')
        else:
          print('Не понял аргумента')
          ws.append([time, targetId, from_id, task, comment, 'ПОСТАВЛЕНА', ''])
      else:
          api.messages.send(access_token=token, peer_id=peer_id, message='Задача "' + task + '" поставлена!')
          ws.append([time, targetId, from_id, task, comment, 'ПОСТАВЛЕНА', ''])
      tasks = []
      wb.save(os.path.join(MEDIA_ROOT, 'TaskBook.xlsx'))
      keyboard = {
        "one_time": False,
        "buttons": []
      }

      for i in range(2, ws.max_row+1):
        if ws[str(columns['id'] + str(i))].value == targetId and ws[str(columns['status'] + str(i))].value not in ['СДЕЛАНО','ОТМЕНЕНО']: 
          t = ws[str(columns['task'] + str(i))].value
          keyboard['buttons'].append([{
            "action": {
              "type": "text",
              "payload": "{\"command\": \"done\", \"task\": \"" + t + "\", \"time\": \"" + ws[str(columns['time'] + str(i))].value + "\"}",
              "label": 'Сделано ' + t
            },
            "color": "positive"
          },
          {
            "action": {
              "type": "text",
              "payload": "{\"command\": \"cancel\", \"task\": \"" + t + "\", \"time\": \"" + ws[str(columns['time'] + str(i))].value + "\"}",
              "label": 'Не могу сделать ' + t
            },
            "color": "negative"
          }
          ])

      kb = ''
      if attach == None or len(attach) == 0:
        api.messages.send(access_token=token, user_id=targetId, message=bossName + ' поставил вам задачу:' + '\n' + task + '\n' + comment, keyboard=json.dumps(keyboard, ensure_ascii = False))
      else:
        a = api.messages.send(access_token=token, user_id=targetId, message=bossName + ' поставил вам задачу:' + '\n' + task + '\n' + comment, keyboard=json.dumps(keyboard, ensure_ascii = False))
      print(args)
      return 'ok'
   except Exception as e:
      print(str(e))
      api.messages.send(access_token=token, peer_id=peer_id, message='Не удалось поставить задачу!')

class Reminder(Thread):
    def __init__(self, event, interval, target_id, bossName, task, comment, token, api):
        Thread.__init__(self)
        self.stopped = event
        self.interval = interval
        self.target_id = target_id
        self.bossName = bossName
        self.task = task
        self.comment = comment
        self.token = token
        self.api = api

    def run(self):
        while not self.stopped.wait(self.interval):
            self.api.messages.send(access_token=self.token, user_id=self.target_id, message='Напоминаю про задачу!\n' + self.bossName + ' отметил ее как важную.\nЗадача: ' +self.task + '\nОписание:' + self.comment)
            # call a function

  
    
    

    
