from django.db import models
from django.forms import ModelForm

class User(models.Model):
    ID = models.IntegerField(primary_key=True)
    NAME = models.CharField(max_length=100)

class Task(models.Model):
    TIME = models.CharField(max_length=30)
    USER_ID = models.OneToOneField(User, related_name="UID", on_delete='cascade')
    BOSS_ID = models.OneToOneField(User,related_name="BID", on_delete='cascade')
    TASK = models.CharField(max_length=100)
    COMMENT = models.CharField(max_length=1000)
    REMINDER = models.CharField(max_length=30)



class Upload(models.Model):
    Критерий = models.FileField(upload_to="media/")
    Достижения = models.FileField(upload_to="media/")
    upload_date=models.DateTimeField(auto_now_add =True)

class UploadTable(models.Model):
    Таблица = models.FileField(upload_to="media/")
    upload_date=models.DateTimeField(auto_now_add =True)

# FileUpload form class.
class UploadForm(ModelForm):
    class Meta:
        model = Upload
        fields = ('Критерий', 'Достижения')

class UploadTableForm(ModelForm):
    class Meta:
        model = UploadTable
        fields = ('Таблица',)
