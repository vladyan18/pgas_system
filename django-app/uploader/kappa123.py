'''
Требования к таблице: 1) отсутствие ячеек, не содержащих категорию достижений или баллы;
                      2) в любой ячейке с категорией содержится хотя бы 1 символ,
                         которого нет в любой ячейке с баллами;
                      3) каждой ячейке с баллами соответствует категория во втором столбце
'''
import json
from openpyxl import load_workbook


def isValue(str): #проверка содержатся ли в ячейке баллы
    if str == None:
        return False
    allowable = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ', '|', ','}
    for symbol in str:
            if symbol not in allowable:
                return False
    return True

def listing(str): #преобразование ячейки с баллами в list
    list = []
    lastIndex = -1
    lastIsComma = False
    lastIsDigit = False
    hadComma = False
    numOfDigit = 1
    for symbol in str:
        if symbol.isdigit() == True:
            if lastIsDigit and hadComma == False:
                list[lastIndex] = list[lastIndex] * 10 + float(symbol)
            elif hadComma == True:
                    list[lastIndex] = list[lastIndex] + float(symbol)/(10**numOfDigit)
                    numOfDigit += 1
            else:
                list.append(float(symbol))
                lastIndex += 1
            lastIsDigit = True
        else:
            if symbol == ',':
                lastIsComma = True
                hadComma = True
            else:
                lastIsComma = False
                hadComma = False
                numOfDigit = 1
            lastIsDigit = False
    return list

def ierarchyAttend(prevRep, currentRepIndex, categoryList, value):
    while currentRepIndex < len(categoryList) - 1:
        prevRep[categoryList[currentRepIndex]] = { }
        prevRep = prevRep[categoryList[currentRepIndex]]
        currentRepIndex += 1
    prevRep[categoryList[currentRepIndex]] = value

def attendInTable(cell, columnIndex, rowIndex, sheet, Table): #добавление ячейки с баллами в словарь
    value = listing(str(cell.value))
    categoryList = []
    numberOfRowCategories = 0
    globalCategoryRowIndex = rowIndex
    while globalCategoryRowIndex > 2 and sheet.cell(row=globalCategoryRowIndex, column=2).value == None:
        globalCategoryRowIndex -= 1
    currentColumnIndex = 2
    globalCategoryLastIndex = 1
    leftColumnIndex = columnIndex
    while currentColumnIndex < columnIndex - 1: #поиск критериев горизонтали
        mergedRowIndex = rowIndex
        while mergedRowIndex > globalCategoryLastIndex and sheet.cell(row=mergedRowIndex, column=currentColumnIndex).value == None:
            mergedRowIndex -= 1
        if globalCategoryLastIndex < mergedRowIndex:
            globalCategoryLastIndex = mergedRowIndex
        if sheet.cell(row=mergedRowIndex, column=currentColumnIndex).value != None and isValue(str(sheet.cell(row=mergedRowIndex, column=currentColumnIndex).value)) == False:
            categoryList.append(" ".join(str(sheet.cell(row=mergedRowIndex, column=currentColumnIndex).value).split()))
            numberOfRowCategories += 1
        if isValue(str(sheet.cell(row=mergedRowIndex, column=currentColumnIndex).value)) == True:
            leftColumnIndex = currentColumnIndex
            break
        currentColumnIndex += 1
    currentRowIndex = rowIndex - 1
    lastCategory = False
    while currentRowIndex >= globalCategoryRowIndex: #поиск критериев по вертикали
        mergedColumnIndex = columnIndex
        while mergedColumnIndex > leftColumnIndex and sheet.cell(row=currentRowIndex, column=mergedColumnIndex).value == None:
            mergedColumnIndex -= 1
        if (sheet.cell(row=currentRowIndex, column=mergedColumnIndex).value != None) and (isValue(str(sheet.cell(row=currentRowIndex, column=mergedColumnIndex).value)) == False):
            categoryList.insert(numberOfRowCategories, " ".join(str(sheet.cell(row=currentRowIndex, column=mergedColumnIndex).value).split()))
            lastCategory = True
        if isValue(str(sheet.cell(row=currentRowIndex, column=mergedColumnIndex).value)) == True and lastCategory == True:
            break
        currentRowIndex -= 1
    prevRep = Table
    currentRepIndex = 0
    while True:
        if categoryList[currentRepIndex] not in prevRep:
            ierarchyAttend(prevRep, currentRepIndex, categoryList, value)
            break
        else:
            prevRep = prevRep[categoryList[currentRepIndex]]
            currentRepIndex += 1
            
def main(book, directory):
    print(book)
    wb = load_workbook(book)

    sheet = wb.get_sheet_by_name('Лист1')

    Table = {}
    rowIndex = 1
    for row in sheet.iter_rows(min_row=1, min_col=2, max_row=542, max_col=296): #перебор всех ячеек таблицы
        columnIndex = 2
        for cell in row:
            if isValue(str(cell.value)):
                attendInTable(cell, columnIndex, rowIndex, sheet, Table)
            columnIndex += 1
        rowIndex += 1

    with open(directory + 'Kriterii.json', 'w', encoding='utf8') as outfile: #экспортирование в json
        json.dump(Table, outfile, ensure_ascii = False)

if __name__ == '__main__':
    main()
